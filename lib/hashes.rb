# EASY

# Define a method that, given a sentence, returns a hash of each of the words as
# keys with their lengths as values. Assume the argument lacks punctuation.
def word_lengths(str)
  length_word = {}

  str.split.each { |word| length_word[word] = word.length }

  length_word
end

# Define a method that, given a hash with integers as values, returns the key
# with the largest value.
def greatest_key_by_val(hash)
  max_key = 0
  max_value = 0

  hash.each do |key, value|
    if value > max_value
      max_key = key
      max_value = value
    end
  end

  max_key
end

# Define a method that accepts two hashes as arguments: an older inventory and a
# newer one. The method should update keys in the older inventory with values
# from the newer one as well as add new key-value pairs to the older inventory.
# The method should return the older inventory as a result. march = {rubies: 10,
# emeralds: 14, diamonds: 2} april = {emeralds: 27, moonstones: 5}
# update_inventory(march, april) => {rubies: 10, emeralds: 27, diamonds: 2,
# moonstones: 5}
def update_inventory(older, newer)
  older.each { |key, value| older[key] = newer[key] }.merge(newer)
end

# Define a method that, given a word, returns a hash with the letters in the
# word as keys and the frequencies of the letters as values.
def letter_counts(word)
  counts = Hash.new(0)

  word.split("").each do |char|
    counts[char] += 1
  end

  counts
end

# MEDIUM

# Define a method that, given an array, returns that array without duplicates.
# Use a hash! Don't use the uniq method.
def my_uniq(arr)
  uniq_arr = []
  counts = Hash.new(0)

  arr.each { |num| counts[num] += 1 }

  counts.each do |key, value|
    uniq_arr << key unless uniq_arr.include?(key)
  end

  uniq_arr
end

# Define a method that, given an array of numbers, returns a hash with "even"
# and "odd" as keys and the frequency of each parity as values.
def evens_and_odds(numbers)
  count_odd = numbers.count { |num| num.odd? }
  count_even = numbers.count { |num| num.even? }

  counts = { even: count_even, odd: count_odd}
end

# Define a method that, given a string, returns the most common vowel. If
# there's a tie, return the vowel that occurs earlier in the alphabet. Assume
# all letters are lower case.
def most_common_vowel(string)
  counts = Hash.new(0)

  string.split("").each do |char|
    counts[char] += 1
  end

  counts.keys.sort.each do |key|
    if counts[key] == counts.values.max
      return key
    end
  end
end

# HARD

# Define a method that, given a hash with keys as student names and values as
# their birthday months (numerically, e.g., 1 corresponds to January), returns
# every combination of students whose birthdays fall in the second half of the
# year (months 7-12). students_with_birthdays = { "Asher" => 6, "Bertie" => 11,
# "Dottie" => 8, "Warren" => 9 }
# fall_and_winter_birthdays(students_with_birthdays) => [ ["Bertie", "Dottie"],
# ["Bertie", "Warren"], ["Dottie", "Warren"] ]
def fall_and_winter_birthdays(students)
  late_birthday_students = []

  late_birthday_keys = students.keys.select { |key| students[key] > 7 }

  (0..(late_birthday_keys.count - 1)).each do |j|
    (0..(late_birthday_keys.count - 1)).each do |i|
      if i > j
      late_birthday_students << [late_birthday_keys[j], late_birthday_keys[i]]
      end
    end
  end

  late_birthday_students
end

# Define a method that, given an array of specimens, returns the biodiversity
# index as defined by the following formula: number_of_species**2 *
# smallest_population_size / largest_population_size biodiversity_index(["cat",
# "cat", "cat"]) => 1 biodiversity_index(["cat", "leopard-spotted ferret",
# "dog"]) => 9
def biodiversity_index(specimens)
  counts = specimens_counts(specimens)

  number_of_species = counts.count
  smallest_population_size = counts.values.min
  largest_population_size = counts.values.max

  number_of_species**2 * smallest_population_size / largest_population_size

end

def specimens_counts(specimens)
  counts = Hash.new(0)

  specimens.each do |char|
    counts[char] += 1
  end

  counts
end

# Define a method that, given the string of a respectable business sign, returns
# a boolean indicating whether pranksters can make a given vandalized string
# using the available letters. Ignore capitalization and punctuation.
# can_tweak_sign("We're having a yellow ferret sale for a good cause over at the
# pet shop!", "Leopard ferrets forever yo") => true
def can_tweak_sign?(normal_sign, vandalized_sign)
  normal_counts = character_count(normal_sign)
  vandalized_counts = character_count(vandalized_sign)
  vandalized_counts.each { |key, value| return true unless value > normal_counts[key] }
  return false
end

def character_count(str)
  counts = Hash.new(0)

  str.split("").each do |char|
    counts[char] += 1
  end

  counts
end
